function sendInvite() {
  var name = document.getElementById("invite-name").value;
  var email = document.getElementById("invite-email").value;

  if (name == "" || email == "") {
    document.getElementById("inviteMessage").innerHTML =
      "<span class='text-success fa-lg'>Please fill up all the fields.</span>";
    document.getElementById("inviteMessage").style.display = "block";
    setTimeout(function() {
      document.getElementById("inviteMessage").style.display = "none";
    }, 4000);
  } else {
    const xhr = new XMLHttpRequest();
    xhr.open(
      "post",
      "https://www.trackmyhealth.in:8443/api/create-invitation/121",
      true
    );
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("apiToken", "0bdbf60a-680e-4ab4-a041-93f5ba94271f");

    let body = {
      invitedToName: name,
      invitedToEmail: email
    };

    xhr.onload = function() {
      let data = JSON.parse(xhr.responseText);
      console.log(data);
      if (data.status.code == 200) {
        document.getElementById("inviteMessage").innerHTML =
          "<span class='text-success fa-lg'>Successfully created.</span>";
        document.getElementById("inviteMessage").style.display = "block";
        setTimeout(function() {
          document.getElementById("inviteMessage").style.display = "none";
          window.location = "#";
        }, 2000);
      } else {
        document.getElementById("inviteMessage").innerHTML =
          "<span class='text-success fa-lg'>Something went wrong!</span>";
        document.getElementById("inviteMessage").style.display = "block";
        setTimeout(function() {
          document.getElementById("inviteMessage").style.display = "none";
        }, 2000);
      }
    };
    xhr.send(JSON.stringify(body));
  }
}
