window.onload = function() {
  let planSection = document.getElementById("planned");
  let progressSection = document.getElementById("in-progress");
  let completedSection = document.getElementById("hello");
  let cancelSection = document.getElementById("cancelled");

  const xhr = new this.XMLHttpRequest();
  xhr.open(
    "GET",
    "https://www.trackmyhealth.in:8443/api/get-all-tasks/121/name/sam",
    true
  );

  xhr.onload = function() {
    let data = JSON.parse(this.response);
    console.log(data);

    data.map(details => {
      console.log(details);
      const planCount = details.planned.length;
      const progressCount = details.progress.length;
      const completeCount = details.completed.length;
      const cancelCount = details.cancelled.length;

      console.log(planCount, progressCount, completeCount, cancelCount);

      // $("#count-planned1, #count-planned2").html("(" + planCount + ")");

      // ==============count===================
      var counT = document.getElementById("count-planned1");
      counT.innerHTML = `(${planCount})`;
      var counT = document.getElementById("count-progress1");
      counT.innerHTML = `(${progressCount})`;
      var counT = document.getElementById("count-complete1");
      counT.innerHTML = `(${completeCount})`;
      var counT = document.getElementById("count-cancelled1");
      counT.innerHTML = `(${cancelCount})`;

      if (planCount > 1) {
        var counT = document.getElementById("count-planned2");
        counT.innerHTML = `(${planCount} tasks)`;
      } else {
        var counT = document.getElementById("count-planned2");
        counT.innerHTML = `(${planCount} task)`;
      }

      if (progressCount > 1) {
        var counT = document.getElementById("count-progress2");
        counT.innerHTML = `(${progressCount} tasks)`;
      } else {
        var counT = document.getElementById("count-progress2");
        counT.innerHTML = `(${progressCount} task)`;
      }

      if (completeCount > 1) {
        var counT = document.getElementById("count-complete2");
        counT.innerHTML = `(${completeCount} tasks)`;
      } else {
        var counT = document.getElementById("count-complete2");
        counT.innerHTML = `(${completeCount} task)`;
      }

      if (cancelCount > 1) {
        var counT = document.getElementById("count-cancelled2");
        counT.innerHTML = `(${cancelCount} tasks)`;
      } else {
        var counT = document.getElementById("count-cancelled2");
        counT.innerHTML = `(${cancelCount} task)`;
      }
      // =======================================

      // ============planned
      details.planned.map(tasks => {
        console.log(tasks);
        console.log(tasks.title);

        var hr1 = document.createElement("hr");

        var mainDiv1 = document.createElement("div");
        mainDiv1.className = "row planned-task1";

        var divL11 = document.createElement("div");
        divL11.className = "col-lg-7";
        var h51 = document.createElement("h5");
        h51.innerHTML = tasks.title;
        var p1 = document.createElement("p");
        p1.innerHTML = `
        Created for ${tasks.personality_name} by ${tasks.user_name}, on
        ${tasks.created_at}
        <br>
        Summary - ${tasks.summary}
        <br>
        Scheduled from ${tasks.task_start_date} till ${tasks.task_end_date}
        `;

        planSection.appendChild(hr1);
        planSection.appendChild(mainDiv1);
        mainDiv1.appendChild(divL11);
        divL11.appendChild(h51);
        divL11.appendChild(p1);

        var divR11 = document.createElement("div");
        divR11.className = "col-lg-5";
        var divR21 = document.createElement("div");
        divR21.className = "view-status";

        var viewBtn1 = document.createElement("div");
        viewBtn1.className = "view-btn ml-lg-auto";
        var a1 = document.createElement("a");
        a1.href = "viewtask.html";
        a1.className = "btn";
        var a_span1 = document.createElement("span");
        a_span1.innerHTML = "VIEW TASK";
        var i1 = document.createElement("i");
        i1.className = "far fa-eye";

        var divR31 = document.createElement("div");
        divR31.className = "task-status d-flex ml-lg-auto";
        var h61 = document.createElement("h6");
        h61.className = "my-auto mr-3";
        h61.innerHTML = "Status";
        var select1 = document.createElement("select");
        select1.className = "custom-select custom-select-sm";
        select1.id = tasks.task_id;
        select1.setAttribute("onchange", "changeStatusPlanned(this)");
        var option11 = document.createElement("option");
        option11.value = "planned";
        option11.innerHTML = "Planned";
        var option21 = document.createElement("option");
        option21.value = "progress";
        option21.innerHTML = "In Progress";
        var option31 = document.createElement("option");
        option31.value = "completed";
        option31.innerHTML = "Completed";
        var option41 = document.createElement("option");
        option41.value = "cancelled";
        option41.innerHTML = "Cancelled";

        mainDiv1.appendChild(divR11);
        divR11.appendChild(divR21);

        divR21.appendChild(viewBtn1);
        viewBtn1.appendChild(a1);
        a1.appendChild(i1);
        a1.appendChild(a_span1);

        divR21.appendChild(divR31);
        divR31.appendChild(h61);
        divR31.appendChild(select1);
        select1.appendChild(option11);
        select1.appendChild(option21);
        select1.appendChild(option31);
        select1.appendChild(option41);
      });
      // ==============================

      // ==============progress
      details.progress.map(tasks => {
        console.log(tasks);
        console.log(tasks.title);

        var hr2 = document.createElement("hr");

        var mainDiv2 = document.createElement("div");
        mainDiv2.className = "row planned-task1";

        var divL12 = document.createElement("div");
        divL12.className = "col-lg-7";
        var h52 = document.createElement("h5");
        h52.innerHTML = tasks.title;
        var p2 = document.createElement("p");
        p2.innerHTML = `
        Created for ${tasks.personality_name} by ${tasks.user_name}, on
        ${tasks.created_at}
        <br>
        Summary - ${tasks.summary}
        <br>
        Scheduled from ${tasks.task_start_date} till ${tasks.task_end_date}
        `;

        progressSection.appendChild(hr2);
        progressSection.appendChild(mainDiv2);
        mainDiv2.appendChild(divL12);
        divL12.appendChild(h52);
        divL12.appendChild(p2);

        var divR12 = document.createElement("div");
        divR12.className = "col-lg-5";
        var divR22 = document.createElement("div");
        divR22.className = "view-status";

        var viewBtn2 = document.createElement("div");
        viewBtn2.className = "view-btn ml-lg-auto";
        var a2 = document.createElement("a");
        a2.href = "viewtask.html";
        a2.className = "btn";
        var a_span2 = document.createElement("span");
        a_span2.innerHTML = "VIEW TASK";
        var i2 = document.createElement("i");
        i2.className = "far fa-eye";

        var divR32 = document.createElement("div");
        divR32.className = "task-status d-flex ml-lg-auto";
        var h62 = document.createElement("h6");
        h62.className = "my-auto mr-3";
        h62.innerHTML = "Status";
        var select2 = document.createElement("select");
        select2.className = "custom-select custom-select-sm";
        select2.id = tasks.task_id;
        select2.setAttribute("onchange", "changeStatusPlanned(this)");
        var option12 = document.createElement("option");
        option12.value = "planned";
        option12.innerHTML = "Planned";
        var option22 = document.createElement("option");
        option22.value = "progress";
        option22.innerHTML = "In Progress";
        var option32 = document.createElement("option");
        option32.value = "completed";
        option32.innerHTML = "Completed";
        var option42 = document.createElement("option");
        option42.value = "cancelled";
        option42.innerHTML = "Cancelled";

        mainDiv2.appendChild(divR12);
        divR12.appendChild(divR22);

        divR22.appendChild(viewBtn2);
        viewBtn2.appendChild(a2);
        a2.appendChild(i2);
        a2.appendChild(a_span2);

        divR22.appendChild(divR32);
        divR32.appendChild(h62);
        divR32.appendChild(select2);
        select2.appendChild(option22);
        select2.appendChild(option12);
        select2.appendChild(option32);
        select2.appendChild(option42);
      });
      // ========================

      // ============completed
      details.completed.map(tasks => {
        console.log(tasks);
        console.log(tasks.title);
        var hr3 = document.createElement("hr");
        var mainDiv3 = document.createElement("div");
        mainDiv3.className = "row planned-task1";
        var divL13 = document.createElement("div");
        divL13.className = "col-lg-7";
        var h53 = document.createElement("h5");
        h53.innerHTML = tasks.title;
        var p3 = document.createElement("p");
        p3.innerHTML = `
        Created for ${tasks.personality_name} by ${tasks.user_name}, on
        ${tasks.created_at}
        <br>
        Summary - ${tasks.summary}
        <br>
        Scheduled from ${tasks.task_start_date} till ${tasks.task_end_date}
        `;

        completedSection.appendChild(hr3);
        completedSection.appendChild(mainDiv3);
        mainDiv3.appendChild(divL13);
        divL13.appendChild(h53);
        divL13.appendChild(p3);

        var divR13 = document.createElement("div");
        divR13.className = "col-lg-5";
        var divR23 = document.createElement("div");
        divR23.className = "view-status";
        var viewBtn3 = document.createElement("div");
        viewBtn3.className = "view-btn ml-lg-auto";
        var a3 = document.createElement("a");
        a3.href = "viewtask.html";
        a3.className = "btn";
        var a_span3 = document.createElement("span");
        a_span3.innerHTML = "VIEW TASK";
        var i3 = document.createElement("i");
        i3.className = "far fa-eye";
        var divR33 = document.createElement("div");
        divR33.className = "task-status d-flex ml-lg-auto";
        var h63 = document.createElement("h6");
        h63.className = "my-auto mr-3";
        h63.innerHTML = "Status";
        var select3 = document.createElement("select");
        select3.className = "custom-select custom-select-sm";
        select3.id = tasks.task_id;
        select3.setAttribute("onchange", "changeStatusPlanned(this)");
        var option13 = document.createElement("option");
        option13.value = "planned";
        option13.innerHTML = "Planned";
        var option23 = document.createElement("option");
        option23.value = "progress";
        option23.innerHTML = "In Progress";
        var option33 = document.createElement("option");
        option33.value = "completed";
        option33.innerHTML = "Completed";
        var option43 = document.createElement("option");
        option43.value = "cancelled";
        option43.innerHTML = "Cancelled";

        mainDiv3.appendChild(divR13);
        divR13.appendChild(divR23);
        divR23.appendChild(viewBtn3);
        viewBtn3.appendChild(a3);
        a3.appendChild(i3);
        a3.appendChild(a_span3);
        divR23.appendChild(divR33);
        divR33.appendChild(h63);
        divR33.appendChild(select3);
        select3.appendChild(option33);
        select3.appendChild(option23);
        select3.appendChild(option13);
        select3.appendChild(option43);
      });
      // ==========================

      // ===========cancelled
      details.cancelled.map(tasks => {
        console.log(tasks);
        console.log(tasks.title);
        var hr4 = document.createElement("hr");
        var mainDiv4 = document.createElement("div");
        mainDiv4.className = "row planned-task1";
        var divL14 = document.createElement("div");
        divL14.className = "col-lg-7";
        var h54 = document.createElement("h5");
        h54.innerHTML = tasks.title;
        var p4 = document.createElement("p");
        p4.innerHTML = `
        Created for ${tasks.personality_name} by ${tasks.user_name}, on
        ${tasks.created_at}
        <br>
        Summary - ${tasks.summary}
        <br>
        Scheduled from ${tasks.task_start_date} till ${tasks.task_end_date}
        `;

        cancelSection.appendChild(hr4);
        cancelSection.appendChild(mainDiv4);
        mainDiv4.appendChild(divL14);
        divL14.appendChild(h54);
        divL14.appendChild(p4);

        var divR14 = document.createElement("div");
        divR14.className = "col-lg-5";
        var divR24 = document.createElement("div");
        divR24.className = "view-status";
        var viewBtn4 = document.createElement("div");
        viewBtn4.className = "view-btn ml-lg-auto";
        var a4 = document.createElement("a");
        a4.href = "viewtask.html";
        a4.className = "btn";
        var a_span4 = document.createElement("span");
        a_span4.innerHTML = "VIEW TASK";
        var i4 = document.createElement("i");
        i4.className = "far fa-eye";
        var divR34 = document.createElement("div");
        divR34.className = "task-status d-flex ml-lg-auto";
        var h64 = document.createElement("h6");
        h64.className = "my-auto mr-3";
        h64.innerHTML = "Status";
        var select4 = document.createElement("select");
        select4.className = "custom-select custom-select-sm";
        select4.id = tasks.task_id;
        select4.setAttribute("onchange", "changeStatusPlanned(this)");
        var option14 = document.createElement("option");
        option14.value = "planned";
        option14.innerHTML = "Planned";
        var option24 = document.createElement("option");
        option24.value = "progress";
        option24.innerHTML = "In Progress";
        var option34 = document.createElement("option");
        option34.value = "completed";
        option34.innerHTML = "Completed";
        var option44 = document.createElement("option");
        option44.value = "cancelled";
        option44.innerHTML = "Cancelled";

        mainDiv4.appendChild(divR14);
        divR14.appendChild(divR24);
        divR24.appendChild(viewBtn4);
        viewBtn4.appendChild(a4);
        a4.appendChild(i4);
        a4.appendChild(a_span4);
        divR24.appendChild(divR34);
        divR34.appendChild(h64);
        divR34.appendChild(select4);
        select4.appendChild(option44);
        select4.appendChild(option34);
        select4.appendChild(option24);
        select4.appendChild(option14);
      });
    });
  };
  xhr.send();
};

function changeStatusPlanned(id) {
  let T_id = id.id;
  var task_id = document.getElementById(id.id);
  var status = task_id.options[task_id.selectedIndex].value;
  console.log(status);
  console.log(task_id);

  const xhr = new XMLHttpRequest();
  xhr.open(
    "PATCH",
    "https://www.trackmyhealth.in:8443/api/update-status-task/" + T_id,
    true
  );
  xhr.setRequestHeader("apiToken", "0bdbf60a-680e-4ab4-a041-93f5ba94271f");
  xhr.setRequestHeader("Content-Type", "application/json");

  let body = {
    status: status
  };

  xhr.onload = function() {
    let data = JSON.parse(xhr.responseText);
    console.log(data);
    if (data.status.code == 200) {
      alert("chnaged");
      window.location.reload();
    }
  };

  xhr.send(JSON.stringify(body));
}

// ======================================================

function createTask() {
  let taskTitle = document.getElementById("task-title").value;
  let taskSummary = document.getElementById("task-summary").value;
  let taskStartDate = document.getElementById("start-date").value;
  let taskEndDate = document.getElementById("end-date").value;
  let rel = document.getElementById("relationship");
  let taskRelationship = rel.options[rel.selectedIndex].text;
  if (
    taskTitle == "" ||
    taskSummary == "" ||
    taskStartDate == "" ||
    taskEndDate == "" ||
    taskRelationship == "Choose"
  ) {
    document.getElementById("showMessAge").innerHTML =
      "<span class='text-success fa-lg'>Please fill up all the fields.</span>";
    document.getElementById("showMessAge").style.display = "block";
    setTimeout(function() {
      document.getElementById("showMessAge").style.display = "none";
    }, 4000);
  } else {
    const xhr = new XMLHttpRequest();
    xhr.open(
      "POST",
      "https://www.trackmyhealth.in:8443/api/create-task-user/121/personality/1",
      true
    );
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("apiToken", "0bdbf60a-680e-4ab4-a041-93f5ba94271f");

    let body = {
      title: taskTitle,
      summary: taskSummary,
      taskStartDate: taskStartDate,
      taskEndDate: taskEndDate,
      relationship: taskRelationship
    };
    xhr.onload = function() {
      let data = JSON.parse(xhr.responseText);
      console.log(data);
      if (data.status.code == 200) {
        document.getElementById("showMessAge").innerHTML =
          "<span class='text-success fa-lg'>Successfully created.</span>";
        document.getElementById("showMessAge").style.display = "block";
        setTimeout(function() {
          document.getElementById("showMessAge").style.display = "none";
          window.location = "#";
        }, 2000);
      } else {
        document.getElementById("showMessAge").innerHTML =
          "<span class='text-success fa-lg'>Something went wrong!</span>";
        document.getElementById("showMessAge").style.display = "block";
        setTimeout(function() {
          document.getElementById("showMessAge").style.display = "none";
        }, 2000);
      }
    };
    xhr.send(JSON.stringify(body));
  }
}

function newTask() {
  $("#planned").hide();
  $("#in-progress").hide();
  $("#hello").hide();
  $("#cancelled").hide();
  $("#icon1").hide();
  $("#icon2").hide();
  $("#icon3").hide();
  $("#icon4").hide();
  $("#new-task").show();
}

function plannedTask() {
  $("#in-progress").hide();
  $("#hello").hide();
  $("#cancelled").hide();
  $("#new-task").hide();
  $("#icon2").hide();
  $("#icon3").hide();
  $("#icon4").hide();
  $("#planned").show();
  $("#icon1").show();
}

function progressTask() {
  $("#planned").hide();
  $("#hello").hide();
  $("#cancelled").hide();
  $("#new-task").hide();
  $("#icon1").hide();
  $("#icon3").hide();
  $("#icon4").hide();
  $("#in-progress").show();
  $("#icon2").show();
}

function completeTask() {
  $("#in-progress").hide();
  $("#planned").hide();
  $("#cancelled").hide();
  $("#new-task").hide();
  $("#icon1").hide();
  $("#icon2").hide();
  $("#icon4").hide();
  $("#hello").show();
  $("#icon3").show();
}

function cancelledTask() {
  $("#in-progress").hide();
  $("#planned").hide();
  $("#hello").hide();
  $("#new-task").hide();
  $("#icon1").hide();
  $("#icon2").hide();
  $("#icon3").hide();
  $("#cancelled").show();
  $("#icon4").show();
}
