// ==============reply-msg===============
let i = 0;

function replyMessage() {
  i++;
  if (i > 0 && i < 2) {
    const repliedText = document.getElementById("replied-text");

    let line = document.createElement("hr");

    let textArea = document.createElement("textarea");
    textArea.name = "reply-message";
    textArea.id = "reply-message";
    textArea.cols = "71";
    textArea.rows = "2";
    textArea.placeholder = "Reply Message";

    let replyButton = document.createElement("button");
    replyButton.type = "button";
    replyButton.className = "btn";
    replyButton.id = "replyId1";
    replyButton.onclick = sendMessage;
    replyButton.innerHTML = "Send";

    repliedText.appendChild(line);
    repliedText.appendChild(textArea);
    repliedText.appendChild(replyButton);
  }
}

function sendMessage() {
  const getReplyMessage = document.getElementById("reply-message").value;
  console.log(getReplyMessage);

  const repliedMessage = document.getElementById("replied-message");

  let line = document.createElement("hr");

  // repliedMessage.className = "ml-3";

  let div = document.createElement("div");
  div.className = "ml-3";
  let row = document.createElement("div");
  row.className = "row";
  let nameLogo = document.createElement("div");
  nameLogo.className = "name-logo";
  let h1 = document.createElement("h1");
  h1.id = "name-letter";
  h1.className = "text-uppercase";
  let nameDetails = document.createElement("div");
  nameDetails.className = "name-details";
  let h5 = document.createElement("h5");
  h5.id = "name";
  let h6 = document.createElement("h6");
  h6.innerHTML = "Feb 29, 3.30 PM";

  let replyMessage = document.createElement("div");
  replyMessage.innerHTML = getReplyMessage;

  repliedMessage.appendChild(line);

  repliedMessage.appendChild(div);

  div.appendChild(row);
  row.appendChild(nameLogo);
  nameLogo.appendChild(h1);
  row.appendChild(nameDetails);
  nameDetails.appendChild(h5);
  nameDetails.appendChild(h6);

  div.appendChild(replyMessage);

  $("#replied-text").hide();
}

const name = document.getElementById("name").innerHTML;

document.getElementById("name-letter").innerHTML = name.slice(0, 1);
